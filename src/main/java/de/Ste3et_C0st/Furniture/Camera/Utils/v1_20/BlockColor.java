package de.Ste3et_C0st.Furniture.Camera.Utils.v1_20;

import org.apache.commons.lang.reflect.MethodUtils;
import org.bukkit.block.Block;

import de.Ste3et_C0st.Furniture.Camera.Utils.MinecraftBlockColor;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;

public class BlockColor extends MinecraftBlockColor{
	
	private static Class<?> CraftMagicNumbersClass;
    
	static {
		try {
			CraftMagicNumbersClass = Class.forName("org.bukkit.craftbukkit." + getBukkitVersion() + ".util.CraftMagicNumbers");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Byte getBlockColor(Block b) {
		try {
			Object nmsBlock = CraftMagicNumbersClass.getMethod("getBlock", org.bukkit.Material.class).invoke(null, b.getType());
			if(FurnitureLib.isVersionOrAbove("1.20.5")) {
				Object MaterialMapColor = MethodUtils.invokeMethod(nmsBlock, "w", null);
				int color = MaterialMapColor.getClass().getField("al").getInt(MaterialMapColor) * 4;
		        return (byte) color;
			}else {
				Object MaterialMapColor = MethodUtils.invokeMethod(nmsBlock, "s", null);
				int color = MaterialMapColor.getClass().getField("al").getInt(MaterialMapColor) * 4;
		        return (byte) color;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
