package de.Ste3et_C0st.Furniture.Camera.Utils.paper;

import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;

import de.Ste3et_C0st.Furniture.Camera.Utils.MinecraftBlockColor;

public class BlockColor extends MinecraftBlockColor{

	@Override
	public Byte getBlockColor(Block b) {
		final BlockData blockData = b.getBlockData();
		return (byte) blockData.getMapColor().asRGB();
	}
	
}
